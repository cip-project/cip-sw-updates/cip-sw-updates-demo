#!/usr/bin/env bash

set -eu

PRODUCT_NAME="bbb-image"
FILES="sw-description sw-description.sig cip-core-image-cip-core-buster-bbb.ext4.img.enc"

cp sw-description.signed-encrypted sw-description
cp cip-core-image-cip-core-buster-bbb.ext4.img.v3.NOT-PANIC cip-core-image-cip-core-buster-bbb.ext4.img

# encrypt
openssl enc -aes-256-cbc -in cip-core-image-cip-core-buster-bbb.ext4.img -out cip-core-image-cip-core-buster-bbb.ext4.img.enc -K $(cat ../scripts/swupdate.encrypt.key | cut -d' ' -f1) -iv $(cat ../scripts/swupdate.encrypt.key | cut -d' ' -f2)

# sign
SHA256=$(sha256sum cip-core-image-cip-core-buster-bbb.ext4.img.enc | cut -d' ' -f1)
sed -i "s/_SHA256_/$SHA256/" sw-description
openssl dgst -sha256 -sign ../scripts/swupdate.priv.pem -passin file:../scripts/passphrase sw-description > sw-description.sig

for i in $FILES;do
	echo $i;done | cpio -ov -H crc > ${PRODUCT_NAME}.v2-v3.SIGNED-ENCRYPTED.swu

rm $FILES
