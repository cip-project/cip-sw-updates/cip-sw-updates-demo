#!/bin/bash -eu

# 1 is enable
if [ $1 -eq 1 ]; then
    sed -i 's%^IMAGE_TYPE ?= "ext4-img"%#IMAGE_TYPE ?= "ext4-img"%' $2
    sed -i 's%^#IMAGE_TYPE ?= "wic-img"%IMAGE_TYPE ?= "wic-img"%' $2
# 0 is disable
elif [ $1 -eq 0 ]; then
    sed -i 's%^#IMAGE_TYPE ?= "ext4-img"%IMAGE_TYPE ?= "ext4-img"%' $2
    sed -i 's%^IMAGE_TYPE ?= "wic-img"%#IMAGE_TYPE ?= "wic-img"%' $2
else
    exit 1
fi
