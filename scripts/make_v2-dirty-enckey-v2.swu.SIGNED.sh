#!/usr/bin/env bash

set -eu

PRODUCT_NAME="bbb-image"
FILES="sw-description sw-description.sig cip-core-image-cip-core-buster-bbb.ext4.img"

cp sw-description.signed sw-description
cp cip-core-image-cip-core-buster-bbb.ext4.img.v2.NOT-PANIC cip-core-image-cip-core-buster-bbb.ext4.img

SHA256=$(sha256sum cip-core-image-cip-core-buster-bbb.ext4.img | cut -d' ' -f1)
sed -i "s/_SHA256_/$SHA256/" sw-description

openssl dgst -sha256 -sign ../scripts/swupdate.priv.pem -passin file:../scripts/passphrase sw-description > sw-description.sig

for i in $FILES;do
	echo $i;done | cpio -ov -H crc > ${PRODUCT_NAME}.v2-dirty-enckey-v2.SIGNED.swu

rm $FILES
