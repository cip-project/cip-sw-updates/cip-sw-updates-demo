#!/usr/bin/env bash

set -eu

PRODUCT_NAME="bbb-image"
FILES="sw-description cip-core-image-cip-core-buster-bbb.ext4.img"

cp sw-description.raw sw-description
cp cip-core-image-cip-core-buster-bbb.ext4.img.v4.NOT-PANIC cip-core-image-cip-core-buster-bbb.ext4.img

for i in $FILES;do
	echo $i;done | cpio -ov -H crc >  ${PRODUCT_NAME}.v3-v4.NOT-PANIC.swu

rm $FILES
