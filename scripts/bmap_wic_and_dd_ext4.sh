#!/bin/bash -u

TARGET_DEV=/dev/sdX

if [ $TARGET_DEV = "/dev/sdX" ]; then
    echo "You have to set an install target device file to $PWD/$(basename $0)."
    exit 1
fi

sudo umount $TARGET_DEV[1-3]

sudo bmaptool copy --bmap cip-core-image-cip-core-buster-bbb.wic.img.bmap cip-core-image-cip-core-buster-bbb.wic.img $TARGET_DEV
sudo dd if=cip-core-image-cip-core-buster-bbb.ext4.img.v1.NOT-PANIC of=${TARGET_DEV}2 bs=32M status=progress
sudo dd if=cip-core-image-cip-core-buster-bbb.ext4.img.v1.NOT-PANIC of=${TARGET_DEV}3 bs=32M status=progress

sync && sync && sync
