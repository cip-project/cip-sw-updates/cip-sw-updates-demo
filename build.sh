#!/bin/bash -eu

###############################################################################
# functions
###############################################################################

usage()
{
    echo "$0 [--avoid-badproxy] {ossj2019|elce2019}"
}

build()
{
    pushd build/isar-cip-core
    ./kas-docker --isar build kas.yml:board-bbb.yml
    popd
}

make_ext4_image()
{
    # comment out recipes-core/customizations/customizations.bb's last paragraph (enable kernel panic)
    scripts/enable_kernel_panic.sh $1 build/isar-cip-core/recipes-core/customizations/customizations.bb

    # change test to true at recipes-core/customizations/files/swupdate.sh
    scripts/enable_test_true.sh $2 build/isar-cip-core/recipes-core/customizations/files/swupdate.sh

    # change chainhandler to $1 at recipes-core/swupdate/files/swupdate_handlers.lua
    scripts/change_chainhandler.sh $3 build/isar-cip-core/recipes-core/swupdate/files/swupdate_handlers.lua

    # change recipes-core/customizations/files/rootfs_version to $2
    scripts/change_rootfs_ver.sh $4 build/isar-cip-core/recipes-core/customizations/files/rootfs_version

    # delete build/tmp/stamps/swupdate-2019.04+-git+isar-r0.do_*
    rm -rf build/isar-cip-core/build/tmp/stamps/swupdate-2019.04+-git+isar-r0.do_*

    # delete build/tmp/stamps/customizations-1.0-r0.do_*
    rm -rf build/isar-cip-core/build/tmp/stamps/customizations-1.0-r0.do_*

    # delete build/tmp/stamps/cip-core-image-1.0-r0.do_ext4_image.*
    rm -rf build/isar-cip-core/build/tmp/stamps/cip-core-image-1.0-r0.do_ext4_image.*

    # build
    build
}

make_ext4_image_and_copy()
{
    make_ext4_image $1 $2 $3 $4

    # copy ext4.img to swu directory and rename it to add ".v2.NOT-PANIC" suffix
    mv build/isar-cip-core/build/tmp/deploy/images/bbb/cip-core-image-cip-core-buster-bbb.ext4.img swu
    mv swu/cip-core-image-cip-core-buster-bbb.ext4.img swu/cip-core-image-cip-core-buster-bbb.ext4.img.$4.$5
}

make_wic_and_bmap()
{
    # change IMAGE_TYPE to ext4-img at conf/machine/bbb.conf
    scripts/enable_wic_img_build.sh 0 build/isar-cip-core/conf/machine/bbb.conf

    # delete build/tmp/stamps/cip-core-image-1.0-r0.do_*
    rm -rf build/isar-cip-core/build/tmp/stamps/cip-core-image-1.0-r0.do_*

    make_ext4_image 0 1 raw v1

    # change IMAGE_TYPE to wic-img at conf/machine/bbb.conf
    scripts/enable_wic_img_build.sh 1 build/isar-cip-core/conf/machine/bbb.conf

    # delete build/tmp/stamps/cip-core-image-1.0-r0.do_*
    rm -rf build/isar-cip-core/build/tmp/stamps/cip-core-image-1.0-r0.do_*

    # build
    build

    # copy wic.img and wic.img.bmap to wic directory
    mkdir -p wic
    cp build/isar-cip-core/build/tmp/deploy/images/bbb/cip-core-image-cip-core-buster-bbb.wic.img* wic

    ## Make v1 (NOT-PANIC) ext4

    # change IMAGE_TYPE to ext4-img at conf/machine/bbb.conf
    scripts/enable_wic_img_build.sh 0 build/isar-cip-core/conf/machine/bbb.conf

    # build
    build

    # copy ext4.img to swu directory and rename it to add ".v1.NOT-PANIC" suffix
    mkdir -p swu
    mv build/isar-cip-core/build/tmp/deploy/images/bbb/cip-core-image-cip-core-buster-bbb.ext4.img swu
    mv swu/cip-core-image-cip-core-buster-bbb.ext4.img swu/cip-core-image-cip-core-buster-bbb.ext4.img.v1.NOT-PANIC
}

make_swu()
{
    # Make v1-v2 (-> NOT-PANIC) raw swu
    cp scripts/make_$1.sh swu
    pushd swu
    ./make_$1.sh
    popd
}

###############################################################################
# main
###############################################################################

if [ $# -eq 1 ]; then
    OPT=
    VERSION=$1
elif [ $# -eq 2 ]; then
    OPT=$1
    VERSION=$2
    if [ $OPT != "--avoid-badproxy" ]; then
        echo "\"$OPT\" is not supported. We support only \"--avoid-badproxy\"."
        exit 1
    fi
else
    usage
    exit 1
fi

if [ $VERSION != "ossj2019" -a $VERSION != "elce2019" ]; then
    echo "\"$VERSION\" is not supported. We support only \"ossj2019\" and \"elce2019\"."
    exit 1
fi

mkdir -p build

pushd build
git clone https://gitlab.com/cip-project/cip-core/isar-cip-core.git
cd isar-cip-core
git checkout cip-sw-updates/swupdate
wget https://raw.githubusercontent.com/siemens/kas/2.2/kas-docker
chmod a+x kas-docker
popd

if [ "$OPT" = "--avoid-badproxy" ]; then
    pushd build/isar-cip-core
    git clone -q https://github.com/ilbers/isar.git
    cd isar
    # Checkout the revision which is written at kas.yml:repos:isar:refspec to patch
    git checkout -q 619d6d88ac8c745282fd16773d50a466567615b6
    cp ../../../scripts/99fixbadproxy meta/recipes-devtools/buildchroot/files
    cp ../../../scripts/99fixbadproxy meta/recipes-core/isar-bootstrap/files
    patch -p1 < ../../../scripts/add_bad_proxy_mesure_to_buildroot.patch
    patch -p1 < ../../../scripts/add_bad_proxy_mesure_to_isar-bootstrap.patch
    popd
fi

pushd build/isar-cip-core
git am ../../patch/0001-SWUpdate-commit-for-demo.patch
popd

if [ $VERSION = "ossj2019" ]; then
    ## Make several images

    # Make v1 (NOT-PANIC) wic and bmap
    make_wic_and_bmap

    # Make ext4 images ($1: enable kernel panic, $2: have test true)
    make_ext4_image_and_copy 0 1 rdiff_image v2 NOT-PANIC    # Make v2 (NOT-PANIC) ext4 (chainhandler=rdiff_image)
    make_ext4_image_and_copy 0 1 raw v3 NOT-PANIC            # Make v3 (NOT-PANIC) ext4
    make_ext4_image_and_copy 1 1 raw v4 PANIC                # Make v4 (PANIC) ext4
    make_ext4_image_and_copy 0 0 raw v4 TEST-FAILURE         # Make v4 (test failure) ext4
    make_ext4_image_and_copy 0 1 raw v4 NOT-PANIC            # Make v4 (NOT-PANIC) ext4

    ## Make delta related files

    # Make v2 (NOT-PANIC) sig
    cp scripts/make_v2.sig.sh swu
    pushd swu
    ./make_v2.sig.sh
    popd

    # Make v2-v3 (-> NOT-PANIC) delta
    cp scripts/make_v2-v3.delta.sh swu
    pushd swu
    ./make_v2-v3.delta.sh
    popd

    # Make v2-v1 (-> Initialize) delta
    cp scripts/make_v2-v1.delta.sh swu
    pushd swu
    ./make_v2-v1.delta.sh
    popd

    ## Make swus

    # Copy sw-description files
    cp scripts/sw-description.* swu

    # Make swu
    make_swu v1-v2.swu.NOT-PANIC    # Make v1-v2 (-> NOT-PANIC) raw swu
    make_swu v2-v3.swu.NOT-PANIC    # Make v2-v3 (-> NOT-PANIC) delta swu
    make_swu v3-v4.swu.PANIC        # Make v3-v4 (-> PANIC) raw swu
    make_swu v3-v4.swu.TEST-FAILURE # Make v3-v4 (-> test failure) raw swu
    make_swu v3-v4.swu.NOT-PANIC    # Make v3-v4 (-> NOT-PANIC) raw swu
    make_swu v2-v1.swu.INIT         # Make v2-v1 (-> Initialize) delta swu
    make_swu v3v4-v1.swu.INIT       # Make v3v4-v1 (-> Initialize) raw swu
elif [ $VERSION = "elce2019" ]; then
    # Apply patches for supporting software update using a signed and encrypted image
    pushd build/isar-cip-core
    git am ../../patch/0002-swupdate-enable-signed-update-images-config.patch
    git am ../../patch/0003-swupdate-add-libssl-dev-to-build-dependencies.patch
    git am ../../patch/0004-swupdate-support-signed-update-image.patch
    git am ../../patch/0005-swupdate-enable-encrypted-update-images-config.patch
    git am ../../patch/0006-swupdate-add-encrypted-update-image-key.patch
    popd

    ## Make keys

    # Make private and public keys to create signed update image
    pushd scripts
    openssl genrsa -aes256 -passout file:passphrase -out swupdate.priv.pem
    openssl rsa -in swupdate.priv.pem -out swupdate.public.pem -outform PEM -pubout -passin file:passphrase
    cp swupdate.public.pem ../build/isar-cip-core/recipes-core/swupdate/files
    popd

    # Make a symmetric key to create an encrypted update image
    pushd scripts
    KEY_OUT=$(openssl enc -aes-256-cbc -k file:passphrase -P -md sha1 -pbkdf2)
    KEY=$(echo "$KEY_OUT" | grep key | cut -d'=' -f2)
    IV=$(echo "$KEY_OUT" | grep iv | cut -d'=' -f2)
    echo "$KEY $IV" > swupdate.encrypt.key
    cp swupdate.encrypt.key ../build/isar-cip-core/recipes-core/swupdate/files
    popd

    # Make a drity symmetric key to show the key can't decrypt an encrypted update image
    pushd scripts
    KEY_OUT=$(openssl enc -aes-256-cbc -k file:passphrase-dirty -P -md sha1 -pbkdf2)
    KEY=$(echo "$KEY_OUT" | grep key | cut -d'=' -f2)
    IV=$(echo "$KEY_OUT" | grep iv | cut -d'=' -f2)
    echo "$KEY $IV" > swupdate.encrypt.key.dirty
    popd

    ## Make several images

    # Make v1 (NOT-PANIC) wic and bmap
    make_wic_and_bmap

    # Make ext4 images ($1: enable kernel panic, $2: have test true)
    make_ext4_image_and_copy 0 1 raw v2 NOT-PANIC               # Make v2 (NOT-PANIC) ext4
    make_ext4_image_and_copy 0 1 raw v2-dirty-subimg NOT-PANIC  # Make v2-dirty-subimg (NOT-PANIC) ext4
    cp scripts/swupdate.encrypt.key.dirty build/isar-cip-core/recipes-core/swupdate/files/swupdate.encrypt.key
    make_ext4_image_and_copy 0 1 raw v2-dirty-enckey NOT-PANIC  # Make v2-dirty-enckey (NOT-PANIC) ext4
    cp scripts/swupdate.encrypt.key build/isar-cip-core/recipes-core/swupdate/files/swupdate.encrypt.key
    make_ext4_image_and_copy 0 1 raw v3 NOT-PANIC               # Make v3 (NOT-PANIC) ext4

    ## Make SWUs

    # Copy sw-description files
    cp scripts/sw-description.* swu

    # Make v1-v2 swu (NOT-SIGNED)
    cp scripts/make_v1-v2.swu.NOT-SIGNED.sh swu
    pushd swu
    ./make_v1-v2.swu.NOT-SIGNED.sh
    popd

    # Make v1-v2-dirty-subimg swu (DIRTY-SUBIMG)
    cp scripts/make_v1-v2.swu.DIRTY-SUBIMG.sh swu
    pushd swu
    ./make_v1-v2.swu.DIRTY-SUBIMG.sh
    popd

    # Make v1-v2 swu (DIRTY-SWDESC)
    cp scripts/make_v1-v2.swu.DIRTY-SWDESC.sh swu
    pushd swu
    ./make_v1-v2.swu.DIRTY-SWDESC.sh
    popd

    # Make v1-v2-dirty-enckey swu (SIGNED)
    cp scripts/make_v1-v2-dirty-enckey.swu.SIGNED.sh swu
    pushd swu
    ./make_v1-v2-dirty-enckey.swu.SIGNED.sh
    popd

    # Make v2-dirty-enckey-v2 swu (SIGNED)
    cp scripts/make_v2-dirty-enckey-v2.swu.SIGNED.sh swu
    pushd swu
    ./make_v2-dirty-enckey-v2.swu.SIGNED.sh
    popd

    # Make v2-v3 swu (SIGNED-ENCRYPTED)
    cp scripts/make_v2-v3.swu.SIGNED-ENCRYPTED.sh swu
    pushd swu
    ./make_v2-v3.swu.SIGNED-ENCRYPTED.sh
    popd

    # Make all-v1 swu (SIGNED)
    cp scripts/make_all-v1.swu.SIGNED.sh swu
    pushd swu
    ./make_all-v1.swu.SIGNED.sh
    popd
else
    echo "$VERSION is not supported."
    exit 1
fi

## Copy swu to the hawkbit directory
cp swu/bbb-image.* hawkbit

## Copy v1 NOT-PANIC ext4 image to wic directory
cp swu/cip-core-image-cip-core-buster-bbb.ext4.img.v1.NOT-PANIC wic

## Copy a burning script to wic directory
cp scripts/bmap_wic_and_dd_ext4.sh wic
