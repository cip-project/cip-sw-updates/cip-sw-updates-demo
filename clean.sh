#!/bin/bash -u

sudo rm -rf                         \
build                               \
wic                                 \
swu                                 \
hawkbit/bbb-image.*.swu             \
scripts/swupdate.priv.pem           \
scripts/swupdate.public.pem         \
scripts/swupdate.encrypt.key        \
scripts/swupdate.encrypt.key.dirty
