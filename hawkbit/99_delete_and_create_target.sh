#!/bin/bash -eu

TMPFILE=$(mktemp)

. ./server.conf

###############################################################################

# Delete a target
curl \
    --basic -u admin:admin \
    'http://'$SERVER'/rest/v1/targets/1' \
    -i \
    -X DELETE \
    2>&1 \
| tee $TMPFILE
echo ""
echo ""
cat $TMPFILE | tail -1 | jq .

###############################################################################

rm -f $TMPFILE

# Create a target
./05_create_new_target.sh
