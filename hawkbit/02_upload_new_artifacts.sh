#!/bin/bash -eu

VERSION=$1

TMPFILE=$(mktemp)

. ./server.conf
. ./swu.conf

ARTIFACT_PREFIX="bbb-image"
ARTIFACT_SUFFIX=".swu"

###############################################################################

upload_new_artifact()
{
    curl \
        --basic -u admin:admin \
        'http://'$SERVER'/rest/v1/softwaremodules/'$1'/artifacts' \
        -i \
        -X POST \
        -H 'Content-Type: multipart/form-data' \
        -F 'file=@'$ARTIFACT_PREFIX$2$ARTIFACT_SUFFIX'' \
        2>&1 \
    | tee $TMPFILE
    echo ""
    echo ""
    cat $TMPFILE | tail -1 | jq .
}

ID=0
while true; do
    ID=$(($ID + 1))
    ARTIFACT_BODY_NAME=$(echo $ARTIFACT_BODY_NAME_LIST | cut -d',' -f$ID)
    if [ -z "$ARTIFACT_BODY_NAME" ]; then
        break
    else
        upload_new_artifact "$ID" "$ARTIFACT_BODY_NAME"
    fi
done

###############################################################################

rm -f $TMPFILE
