#!/usr/bin/env bash

# You do not have to worry about the execution location
cd $(dirname $0)

# Fail on unset variables and command errors
set -ue -o pipefail

# Prevent commands misbehaving due to locale differences
export LC_ALL=C

# Output green characters
pr_green() {
    echo -en "\e[32m"
    echo $1
    echo -en "\e[m"
}

# Output red characters
pr_red() {
    echo -en "\e[31m"
    echo $1
    echo -en "\e[m"
}

###############################################################################

docker run -d -p 8083:8080 hawkbit/hawkbit-update-server
if [ $? -eq 0 ]; then
    pr_green 'docker run scceeded. Access "localhost:8083" via a web browser. Hawkbit login account is admin/admin.'
else
    pr_red "docker run failed."
fi
