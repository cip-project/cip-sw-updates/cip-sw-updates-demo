#!/bin/bash -eu

if [ $# -ne 1 ]; then
    echo "You have to add an argument: \"ossj2019\" or \"elce2019\""
    exit 1
fi
VERSION=$1
if [ $VERSION != "ossj2019" -a $VERSION != "elce2019" ]; then
    echo "\"$VERSION\" is not supported. We support only \"ossj2019\" and \"elce2019\"."
    exit 1
fi

# Execute only once before starting a demo day
./00_initial_setting.sh
./01_create_new_software_modules.sh $VERSION
./02_upload_new_artifacts.sh $VERSION
./03_make_new_distribution_sets.sh $VERSION
./04_assign_a_software_modules_to_a_distribution_set.sh
./05_create_new_target.sh
./06_change_polling_time.sh
